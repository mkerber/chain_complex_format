#include <scc/Scc.h>
#include <fstream>

int main(int argc, char** argv) {
  std::string s = "0.21 0.7 0.513  1543:0 3:-1 4:3 5:7.1    77  41:-5.41  12:abc";
  scc::Column_data col;
  parse_line_to_column_data(s,3,col);

  const char* filename = (argc>=2 ? argv[1] : "example.txt");

  {
    
    std::ifstream ifstr(filename);
    
    scc::Scc<> parser(ifstr);
    
    int no_levels = parser.has_grades_on_last_level() ? parser.number_of_levels() : parser.number_of_levels()-1;
    
    std::cout << "Number levels: " << no_levels << std::endl;
    
    for(int i=1;i<=no_levels;i++) {
      for(int j=0;j<parser.number_of_generators(i);j++) {
	std::cout << i << " " << j << ": " << std::flush;
	std::vector<double> grades;
	std::vector<std::pair<int,int> > bd;
	parser.next_column(i,std::back_inserter(grades),std::back_inserter(bd));
	std::cout << "Grades: " << std::flush;
	for(double d : grades) {
	  std::cout << d << " ";
	}
	std::cout << "Boundary: " << std::flush;
	for(auto p : bd) {
	  std::cout << p.first << ":" << p.second << " ";
	}
	std::cout << std::endl;
      }
      
    }
  }


}
