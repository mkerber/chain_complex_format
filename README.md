### The ccfmgm2020 format ###

This document specifies a data format to define
**c**hain **c**omplexes of **f**ree **m**ulti-**g**raded **m**odules.
It has been defined through discussions between Michael Kerber
and Michael Lesnick in 2020.


### The format ###

We refer to the file **format.pdf** in this repository for the precise
specification of the format.

### A parser ###

The include directory contains a simple parser to read files in scc2020
format and provides a simple interface to iterate through the chain complex
at different levels. This parser is used in the libraries [mpfree](https://bitbucket.org/mkerber/mpfree) and [multi-chunk](https://bitbucket.org/mkerber/multi_chunk). The parser does currently not support the reverse option.

### Contact ###

Michael Kerber (kerber@tugraz.at)
Michael Lesnick (lesnick@gmail.com)
